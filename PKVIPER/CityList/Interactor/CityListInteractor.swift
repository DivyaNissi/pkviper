//
//  CityListInteractor.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListInteractor: CityListInputInteractorProtocol {
    
    weak var presenter: CityListOutputInteractorProtocol?
    
    func getCityList() {
        presenter?.cityListDidFetch(cityList: getAllCityDetail())
    }
    
    func getAllCityDetail() -> [City] {
        var cityList = [City]()
        let allCityDetail = Common.generateDataList()
        for item in allCityDetail {
            cityList.append(City(attributes: item))
        }
        return cityList
    }
}
