//
//  Common.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class Common: NSObject {
    
    class func generateDataList() -> [[String: String]] {
        return [["name": "Bhubaneshwar","population": "8.38 lakhs"," image": "Bhubaneshwar"],["name": "Pune","population": "31.2 lakhs","image": "Pune"], ["name": "Jaipur","population": "30.7 lakhs","image": "Jaipur"], ["name": "Surat","population": "44.6 lakhs","image": "Surat"],["name": "Kochi","population": "21.2 lakhs","image": "Kochi"],["name": "Ahmedabad","population": "55.7 lakhs","image": "Ahmedabad"],["name": "Jabalpur","population": "12.7 lakhs","image": "Jabalpur"],["name": "Vishakhapatnam","population": "30 lakhs","image": "Vishakhapatnam"],["name": "Solapur","population": "9.51 lakhs","image": "Solapur"],["name": "Davanagere","population": "4.35 lakhs","image": "Davanagere"]]
    }
    
}
