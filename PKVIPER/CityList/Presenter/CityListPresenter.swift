//
//  CityListPresenter.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListPresenter: CityListPresenterProtocol {
    
    var wireframe: CityListWireFrameProtocol?
    weak var view: CityListViewProtocol?
    var interactor: CityListInputInteractorProtocol?
    
    func showCitySelection(with city: City, from view: UIViewController) {
        wireframe?.pushToCityDetail(with: city, from: view)
    }
    
    func viewDidLoad() {
        self.loadCityList()
    }

    func loadCityList() {
        interactor?.getCityList()
    }
    
}

extension CityListPresenter: CityListOutputInteractorProtocol {
    
    func cityListDidFetch(cityList: [City]) {
        view?.showCity(with: cityList)
    }
    
}
