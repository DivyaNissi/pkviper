//
//  CityListCell.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListCell: UITableViewCell {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var populationLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
