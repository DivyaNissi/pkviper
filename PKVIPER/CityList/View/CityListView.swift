//
//  CityListView.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityListView: UIViewController,CityListViewProtocol {
        
        @IBOutlet var cityTblView: UITableView!
        
        var presenter:CityListPresenterProtocol?
        var cityList = [City]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "City List"
            CityListWireframe.createCityListModule(cityListRef: self)
            presenter?.viewDidLoad()
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }

        func showCity(with city: [City]) {
            cityList = city
            cityTblView.reloadData()
        }

    }

    extension CityListView: UITableViewDataSource, UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = cityTblView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
            let city = cityList[indexPath.row]
            cell.textLabel?.text = city.name
            cell.detailTextLabel?.text = city.population
            return cell
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cityList.count
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            presenter?.showCitySelection(with: cityList[indexPath.row], from: self)
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return UITableView.automaticDimension
         }
        
    }


