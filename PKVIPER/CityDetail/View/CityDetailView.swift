//
//  CityDetailView.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityDetailView: UIViewController, CityDetailViewProtocol {

    @IBOutlet var cityImage: UIImageView!
    @IBOutlet var cityNameLbl: UILabel!
    @IBOutlet var population: UILabel!
    
    var presenter: CityDetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showCityDetail(with city: City) {
        title = city.name
        cityImage.image = UIImage(named: city.name)
        cityNameLbl.text = city.name
        population.text = city.population
    }
    
    deinit {
        print("CityDetailView removed")
    }

}
