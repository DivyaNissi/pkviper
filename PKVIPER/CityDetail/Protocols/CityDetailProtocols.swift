//
//  CityDetailPresenterProtocol.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

protocol CityDetailPresenterProtocol: class {
    
    var wireframe: CityDetailWireFrameProtocol? {get set}
    var view: CityDetailViewProtocol? {get set}
    
    //View -> Presenter
    func viewDidLoad()
    func backButtonPressed(from view: UIViewController)
    
}

protocol CityDetailViewProtocol: class {
    //Presenter -> View
    func showCityDetail(with city: City)
}

protocol CityDetailWireFrameProtocol: class {
    func goBackToCityListView(from view: UIViewController)
}
