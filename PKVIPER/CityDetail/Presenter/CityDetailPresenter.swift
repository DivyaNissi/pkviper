//
//  CityDetailPresenter.swift
//  PKVIPER
//
//  Created by Divya Kothagattu on 27/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class CityDetailPresenter: CityDetailPresenterProtocol {
    
    weak var view: CityDetailViewProtocol?
    var wireframe: CityDetailWireFrameProtocol?
    var city: City?
    
    func viewDidLoad() {
        view?.showCityDetail(with: city!)
    }
    
    func backButtonPressed(from view: UIViewController) {
        
    }
    
    deinit {
        print("CityDetailPresenter removed")
    }
    
}
